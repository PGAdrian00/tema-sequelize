const express = require("express");
const connection = require("./models").connection;
const router = require("./routes");
const app = express();

app.use(express.json());

let port = 8081;

app.use("/api", router);

app.get("/reset", (req, res) => {
  connection
    .sync({ force: true })
    .then(() => {
      res.status(201).send({ message: "Database reset" });
    })
    .catch(() => {
      res.status(500).send({ message: "Database reset failed" });
    });
});

app.use("/*", (req, res) => {
  res.status(200).send("App ruleaza");
});

app.listen(port, () => {
  console.log("Server is running on " + port);
});

//delete
app.delete("/crocodili/:id", (req, res) => {
  try {
    const sqlDelete = `DELETE FROM Crocodili WHERE id = '${req.params.id}'`;
    connection.query(sqlDelete, (err) => {
      if (err) throw err;
      res.status(200).send({ message: "Crocodil disparut" });
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});

//update
app.put("/crocodili/:id", (req, res) => {
  try {
      const sqlPut = `UPDATE crocodili Set nume = '${req.body.nume}', prenume = '${req.body.prenume}', email = '${req.body.email}', telefon = '${req.body.telefon}', activ = '${req.body.activ}' WHERE id = '${req.params.id}'`;
    connection.query(sqlPut, (err) => {
      if (err) throw err;
      res.status(200).send({ message: "Crocodil schimbat" });
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});
